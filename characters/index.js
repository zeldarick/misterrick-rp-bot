module.exports = Character;

function Character () {
    this.name = '';
}
Character.commands = ['/npc', '/pc'];

Character.prototype.getResult = function (words) {
    var result = '';
    this.name = words.shift();
    if (words[0] === 'says') {
        result = this.formatSpeech(words.slice(1));
    } else {
        result = this.formatAction(words);
    }

    return result;
};

Character.prototype.formatSpeech = function (speech) {
    return '**' + this.name + ':** ' + speech.join(' ');
};

Character.prototype.formatAction = function (action) {
    return '***' + this.name + '*** *' + action.join(' ') + '*';
};