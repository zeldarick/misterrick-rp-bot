var Character = require('./characters');
var Discord = require('discord.js');
var Dice = require('./dice');

var token = process.env.DISCORD_TOKEN;

var bot = new Discord.Client();

//when the bot is ready
bot.on('ready', () => {
    console.log(`Ready to begin! Serving in ${bot.channels.size} channels`);
});

//when the bot disconnects
bot.on('disconnected', () => {
    //alert the console
    console.log('Disconnected!');

    //exit node.js with an error
    process.exit(1);
});

function shouldRespond (msg) {
    return (msg.channel.name === 'testing' && !process.env.PRODUCTION) ||
           (msg.channel.name !== 'testing' && process.env.PRODUCTION);
}

function sendMessage(msg, content, options = {}) {
    msg.reply(content, options);
}

function handleError (msg, error) {
    console.log('There was an error:', error);
    if (error.stack) {
        console.log(error.stack);
    }
    sendMessage(msg, 'Sorry, there was an error: ' + error.message);
}

//when the bot receives a message
bot.on('message', msg => {
    if (!shouldRespond(msg)) return;

    /**
     *  Dice Commands
     */
    if (msg.content.startsWith(Dice.command)) {
        var dice = new Dice();

        try {
            var result = dice.getResult(msg);
        } catch (error) {
            handleError(msg, error);
        }

        msg.reply(result);
        console.log(msg.author.username, 'said', msg.content, 'replied with', result);
        return;
    }

    /**
     * Character Commands
     */
    var words = msg.content.split(' ');
    if (Character.commands.indexOf(words.shift()) !== -1) {
        var character = new Character();
        try {
            var result = character.getResult(words);
        } catch (error) {
            handleError(msg, error);
        }

        console.log('Changing', msg.content, 'to', result);
        // For some reason, editing messages results in a permissions error
        // bot.updateMessage(msg, result, {}, function (error, msg) {
        //     console.log('error: ', error);
        //     console.log('msg:', msg);
        // });
        // But we do have permission to delete messages, so this will do for now...
        sendMessage(result);
        bot.deleteMessage(msg);
    }
});

bot.login(token)
.catch(err => {
    console.log('Login failed: ', err.message);
});
