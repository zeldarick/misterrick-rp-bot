var _ = require('lodash');
var BasicDice = require('./BasicDice');
var Modifier = require('./Modifier');
var PercentageDie = require('./PercentageDie');
var HigherLowerDice = require('./HigherLowerDice');
var StatDice = require('./StatDice');
var parseDiceExpression = require('./common').parseDiceExpression;

module.exports = Dice;

/**
 * Handles dice commands
 */
function Dice () {
    this.dice = null;
    this.mod = null;
    this.message = '';
    this.roller = '';
}
Dice.command = '/roll';

Dice.prototype.getResult = function (message) {
    this.roller = message.author.username;
    var offset = Dice.command.length;
    var messageContent = message.content.slice(offset);
    this.message = _.trim(messageContent).toLowerCase();
    this.parse(this.message);
    this.dice.roll();
    return this.toString();
};

Dice.prototype.parse = function (message) {
    // Percentage die
    if (message === 'percent' ||
        message === 'pertentage' ||
        message === '%') {
        this.dice = new PercentageDie();
    }
    // Stat dice, 4d6, sum the highest 3
    else if (message === 'stat') {
        this.dice = new StatDice();
    }
    // Advantage or disadvantage (with a modifier)
    else if (_.startsWith(message, 'adv') ||
             _.startsWith(message, 'disadv') ||
             _.startsWith(message, 'dis')) {
        this.dice = new HigherLowerDice();
        this.dice.parse(this.message);
    }
    // Basic expression, e.g. 3d6
    else {
        var expression = parseDiceExpression(message);
        this.dice = new BasicDice(expression.quantity, expression.sides);
        if (expression.mod) {
            var mod = new Modifier(expression.mod.operator, expression.mod.value);
            this.dice.addModifier(mod);
        }
    }

    return this;
};

Dice.prototype.toString = function () {
    var output = this.roller + ' rolled ' + this.message + "\n";
    return output + this.dice.toString();
};
