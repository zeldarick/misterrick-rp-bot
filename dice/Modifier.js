var BasicDice = require('./BasicDice');
var parseDiceExpression = require('./common').parseDiceExpression;

module.exports = Modifier;

function Modifier (operator, value) {
    this.operator = operator || '+';

    if (value && this.isDiceValue(value)) {
        this.value = this.getDiceTotal(value);
    } else {
        this.value = parseInt(value) || 0;
    }
}

Modifier.prototype.setOperator = function (operator) {
    this.operator = operator;
    return this;
};

Modifier.prototype.setValue = function (value) {
    if (this.isDiceValue(value)) {
        this.value = this.getDiceTotal(value);
    } else {
        this.value = parseInt(value);
    }
    return this;
};

Modifier.prototype.getDiceTotal = function (expression) {
    var exp = parseDiceExpression(expression);
    var dice = new BasicDice(exp.quantity, exp.sides);
    return dice.roll().sum();
}

Modifier.prototype.applyTo = function (value) {
    var diff = this.value;
    if (this.operator === '-') {
        diff = -diff;
    }
    
    return value + diff;
};

Modifier.prototype.isDiceValue = function (value) {
    return value.indexOf('d') !== -1;
};

Modifier.prototype.toString = function () {
    var output = ' ' + this.operator + ' ';
    output += this.value.toString();
    return output;
};
