var _ = require('lodash');

module.exports = BasicDie;

/**
 * A single basic die with X sides.
 * @param {Number} sides  The number of sides this die has
 */
function BasicDie (sides) {
    this.sides = sides || 6;
    this.result = null;
    this.highlightMax = this.sides === 20;
    this.highlightMin = this.sides === 20;
}

BasicDie.prototype.roll = function () {
    this.result = _.random(1, this.sides);
    return this;
};

BasicDie.prototype.shouldHighlight = function () {
    return ((this.highlightMax && this.result === this.sides) ||
            (this.highlightMin && this.result === 1));
};

BasicDie.prototype.toString = function () {
    var result = this.result.toString();
    // __**message**__ will make message bold and underlined in discord.
    if (this.shouldHighlight()) {
        result = '__**' + result + '**__';
    }
    return result;
};
