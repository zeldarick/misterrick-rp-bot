var _ = require('lodash');
var BasicDie = require('./BasicDie');

module.exports = BasicDice;

/**
 * A collection of basic dice
 * @param {Number} quantity  The number of dice in this collection.
 * @param {Number} size  The number of sides each die has.
 */
function BasicDice (quantity, sides) {
    this.quantity = quantity || 1;
    this.sides = sides || 20;
    this.dice = [];
    this.mod = null;

    // Create our dice
    _.times(this.quantity, _.bind(function (index) {
        this.dice.push(new BasicDie(this.sides));
    }, this));
}

/**
 * Rolls all the dice in the collection.
 */
BasicDice.prototype.roll = function () {
    this.dice.forEach(function (die) {
        die.roll();
    });
    return this;
};

/**
 * Adds a modifier to this collection.
 * @param {Number} mod  The amount to add.
 */
BasicDice.prototype.addModifier = function (mod) {
    this.mod = mod;
    return this;
};

/**
 * Finds the total of all the dice results.
 */
BasicDice.prototype.sum = function () {
    return _(this.dice).map('result').sum();
};

/**
 * Sorts the dice into ascending order.
 */
BasicDice.prototype.sort = function () {
    return this.dice = _.sortBy(this.dice, 'result');
};

/**
 * Outputs a dice message representing this collection.
 */
BasicDice.prototype.toString = function () {
    var results = _.map(this.dice, function (die) {
        return die.toString();
    });
    var total = this.sum();

    var output = '(';
    output += results.join(', ');
    output += ')';
    if (this.mod) {
        total = this.mod.applyTo(total);
        output += this.mod.toString();
    }
    output += ' Total: ' + total;
    return output;
};
