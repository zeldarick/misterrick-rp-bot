module.exports.parseDiceExpression = parseDiceExpression;

var DICE_REGEX = /([0-9]*)d([0-9]*) *([+|-])* *(.*)/i;

function parseDiceExpression (expression) {
    var matches = expression.match(DICE_REGEX);
    if (!matches || !matches.length > 1) {
        console.log('Failed to match', expression);
        throw new Error('Unrecognised dice expression');
    }

    var quantity    = matches[1] || 1;
    var sides       = matches[2];
    var modOperator = matches[3] || null;
    var modValue    = matches[4] || null;

    var mod = null;
    if (modOperator && modValue) {
        mod = {
            operator: modOperator,
            value: modValue
        };
    }

    return {
        quantity: quantity,
        sides: sides,
        mod: mod
    };
}