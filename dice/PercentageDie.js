var _ = require('lodash');

module.exports = PercentageDie;

/**
 * A dice roll representing a percentage.
 */
function PercentageDie () {
    this.sides = 100;
    this.result = null;
}

PercentageDie.prototype.roll = function () {
    this.result = _.random(1, this.sides);
    return this;
};

PercentageDie.prototype.toString = function () {
    return this.result.toString() + '%';
};
