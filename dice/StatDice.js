var _ = require('lodash');
var BasicDice = require('./BasicDice');

module.exports = StatDice;

/**
 * Rolls 4d6 and sums the highest 3.
 */
function StatDice () {
    BasicDice.call(this, 4, 6);

    this.highest = [];
}
StatDice.prototype = Object.create(BasicDice.prototype);

/**
 * Finds the highest 3 dice and sets them to this.highest.
 */
StatDice.prototype.findHighest = function () {
    this.sort();
    this.highest = this.dice.slice(1);
};

/**
 * Outputs a dice message representing this collection.
 */
StatDice.prototype.toString = function () {
    var results = _.map(this.dice, function (die) {
        return die.toString();
    });
    this.findHighest();
    var highest = _.map(this.highest, function (die) {
        return die.toString();
    });
    var total = _(this.highest).map('result').sum();

    var output = '(';
    output += results.join(', ');
    output += ')';
    output += ' Highest (';
    output += highest.join(', ');
    output += ') Stat: ' + total;
    return output;
};
