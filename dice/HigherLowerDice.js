var _ = require('lodash');
var BasicDie = require('./BasicDie');
var Modifier = require('./Modifier');

module.exports = HigherLowerDice;

var REGEX = /(adv|dis|disadv)\s*([+|-]*)\s*(\d*)/i;

/**
 * A collection of X dice, with the highest or lowser being highlighted
 * @param {Number} [quantity=2]  The number of dice in this collection.
 * @param {Number} [sides=20]  The number of sides each die in this collection has.
 * @param {String} [type='higher']  Can be either 'higher' or 'lower'.
 */
function HigherLowerDice (quantity, sides, type) {
    this.quantity = quantity || 2;
    this.sides = sides || 20;
    this.type = type || 'higher';
    this.dice = [];
    this.mod = null;

    // Create our dice
    _.times(this.quantity, _.bind(function (index) {
        this.dice.push(new BasicDie(this.sides));
    }, this));
}

HigherLowerDice.prototype.parse = function (string) {
    var matches = string.match(REGEX);
    if (!matches || matches.length < 1) {
        console.log('Failed to match', expression);
        throw new Error('Unrecognised dice expression');
    }

    var type        = matches[1];
    var modOperator = matches[2] || null;
    var modValue    = matches[3] || null;

    this.type = type === 'adv' ? 'higher' : 'lower';
    if (modOperator) {
        var mod = new Modifier(modOperator, modValue);
        this.addModifier(mod);
    }
};

HigherLowerDice.prototype.addModifier = function (mod) {
    this.mod = mod;
};

HigherLowerDice.prototype.roll = function () {
    this.dice.forEach(function (die) {
        die.roll();
    });
    return this;
};

HigherLowerDice.prototype.toString = function () {
    var results = _.map(this.dice, 'result');
    var usedDie = this.type === 'higher' ? _.max(results) : _.min(results);
    var output = '(' + results.join(', ') + ') ';
    output += this.type === 'higher' ? 'Highest: ' : 'Lowest: ';
    output += usedDie;
    if (this.mod) {
        var total = this.mod.applyTo(usedDie);
        output += ' ' + this.mod.operator + ' ' + this.mod.value;
        output += ' Total: ' + total;
    }
    return output;
};
